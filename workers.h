#ifndef WORKERS_H
#define WORKERS_H

#include "ipmi_types.h"
#include "ftb_publisher.h"
#include "tasklist.h"

int start_workers( ipmi_config_t* ipmi_config, ipmi_param_t * ipmi_param, ftb_context_t* ftb_context, tasklist_t *tasklist );
int stop_workers();


#endif
