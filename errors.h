#ifndef ERRORS_H
#define ERRORS_H

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <ipmi_monitoring.h>




// Common print function
#define _COMMON_PRINT_( FMT, args... ) \
do { fprintf( stderr, "[%s:%d][%s] "FMT, __FILE__, __LINE__, __func__, ##args ); } while(0)


// Print an error message 
#define PRINT_ERROR( FMT, args... ) \
do { _COMMON_PRINT_( FMT, ##args ); } while(0)

    
// Print an error message with the errno error message
#define MAX_ERR_MSG 200
#define PRINT_ERROR_ERRNO( FMT, ERRCODE, args... )                \
do {                                                              \
  char err_msg[MAX_ERR_MSG];                                      \
  strerror_r( ERRCODE, err_msg, MAX_ERR_MSG );                    \
  _COMMON_PRINT_( FMT": %s (%d)\n", ##args, err_msg, ERRCODE );   \
} while(0)


// Print an error message with the IPMI error message
#define PRINT_IPMI_ERROR( FMT, ctx, args... )                     \
do {                                                              \
  int __errnum = ipmi_monitoring_ctx_errnum( ctx );               \
  char* __errstr = ipmi_monitoring_ctx_strerror( __errnum );      \
  _COMMON_PRINT_( FMT": %s (%d)\n", ##args, __errstr, __errnum ); \
} while (0)


// Check assertion and if failed:
// - print an error message 
// - abort
#define ASSERT_MSG( COND, FMT, args... )   \
do {                                       \
    if( !(COND) ) {                        \
        _COMMON_PRINT_( FMT, ##args );     \
        abort();                           \
}} while(0)

#define ASSERT_MSG_ERRNO( COND, FMT, ERRCODE, args... )   \
do {                                                      \
    if( !(COND) ) {                                       \
        PRINT_ERROR_ERRNO( FMT, ERRCODE, ##args );        \
        abort();                                          \
}} while(0)



#endif
