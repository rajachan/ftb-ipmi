#include "arguments.h"
#include "errors.h"
#include "ftb-ipmi.h"
#include <argp.h>

// const char *argp_program_version = "";
// const char *argp_program_bug_address = "";

// Program documentation
static char doc[] = "FTB-IPMI: publish IPMI event to FTB";

// A description of the arguments we accept
static char args_doc[] = "";

// The options we understand.
static struct argp_option options[] = {
    {"verbose"      , 'v', 0              , 0, "Produce verbose output" },
    {"timer"        , 't', 0              , 0, "Print timing information" },
    {"username"     , 'u', "USERNAME"     , 0, "Username to use when authenticating with the remote host" },
    {"password"     , 'p', "PASSWORD"     , 0, "Password to use when authenticating with the remote host" },
    {"hostfile"     , 'f', "HOSTFILE"     , 0, "File that contains the list of IPMI hosts to monitor" },
    {"nb-threads"   , 'n', "NB_THREADS"   , 0, "Size of the thread pool for the IPMI requests" },
    {"nb-iterations", 'i', "NB_ITERATIONS", 0, "Number of iterations (0 means iterate forever)" },
    {"delay"        , 'd', "DELAY"        , 0, "Delay time interval between each set of queries" },
    { 0 }
};

// Parse a single option. 
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    // Get the input argument from argp_parse,
    // which we know is a pointer to our arguments structure
    arguments_t *arguments = state->input;
    
    switch (key) {
        case 'v':
            arguments->verbose++;
            break;

        case 't':
            arguments->timer = 1;
            break;

        case 'f':
            arguments->hostfile = arg;
            break;

        case 'u':
            arguments->username = arg;
            break;

        case 'p':
            arguments->password = arg;
            break;

        case 'n':
        {
            int n = atoi( arg );
            if ( n < 0 ) {
                printf("Error: the number of threads must be positive\n");
                argp_usage( state );
            }
            arguments->nb_threads = n;
            break;
        }

        case 'i':
        {
            int i = atoi( arg );
            if ( i < 0 ) {
                printf("Error: the number of iterations must be positive or null\n");
                argp_usage( state );
            }
            arguments->nb_iterations = i;
            break;
        }

        case 'd':
        {
            int d = atoi( arg );
            if ( d < 0 ) {
                printf("Error: the delay time must be positive or null\n");
                argp_usage( state );
            }
            arguments->delay = d;
            break;
        }

        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

// Our argp parser. 
static struct argp argp = { options, parse_opt, args_doc, doc };

void initialize_arguments( arguments_t *arguments ) {
    arguments->verbose = 0;
    arguments->timer = 0;
    arguments->hostfile = NULL;
    arguments->username = NULL;
    arguments->password = NULL;
    arguments->nb_threads = 0;
    arguments->nb_iterations = 0;
    arguments->delay = 60;
}

// Print the content of struct arguments
void print_arguments( arguments_t* arguments ) {
    printf ("VERBOSE = %d\n",    arguments->verbose  );
    printf ("TIMER = %s\n",      arguments->timer ? "yes" : "no" );
    printf ("HOSTFILE = %s\n",   arguments->hostfile );
    printf ("USERNAME = %s\n",   arguments->username );
    printf ("PASSWORD = %s\n",   arguments->password );
    printf ("NB_THREADS = %d\n", arguments->nb_threads );
    printf ("NB_ITERATIONS = %d\n", arguments->nb_iterations );
    printf ("DELAY = %d s\n",    arguments->delay );
}

// Parse arguments from the command line and fill the struct arguments
int parse_arguments( int argc, char **argv, arguments_t* arguments ) {

    initialize_arguments( arguments );

    // Parse our arguments
    int rv = argp_parse( &argp, argc, argv, 0, 0, arguments );
    if ( rv != 0 ) {
        PRINT_ERROR_ERRNO( "argp_parse", rv );
        return rv;
    }

    return 0;
}

