#include "tasklist.h"
#include "errors.h"
#include <pthread.h>

// =======================================
// Task internal functions
// =======================================


int task_init( task_t *task, const char* hostname )
{
    ASSERT_MSG( task != NULL, "Error: task is NULL\n" );
    task->hostname = strdup( hostname );
    if ( task->hostname == NULL) {
        PRINT_ERROR_ERRNO("strdup", errno);
        return -1;
    }
    task->sensor_num = 0;
    task->sensor_state = NULL;
    return 0;
}


void task_clean( task_t *task )
{
    ASSERT_MSG( task != NULL, "Error: task is NULL\n" );
    free( task->hostname );
}


// =======================================
// Task List 
// =======================================


struct tasklist {
    task_t* task;
    unsigned int size;
    unsigned int current_index;
    pthread_cond_t cond_not_empty;
    pthread_mutex_t mutex;

    // Completed tasks
    unsigned int completed;
    pthread_cond_t cond_completed;
};


// Allocate and create a new taslist based on hostlist
tasklist_t* tasklist_create( hostlist_t *hostlist )
{
    int rv;
    ASSERT_MSG( hostlist != NULL, "Error: hostlist is NULL\n" );

    tasklist_t* tasklist = NULL;
    
    // Allocate tasklist
    tasklist = malloc( sizeof(tasklist_t) );
    ASSERT_MSG_ERRNO( tasklist != NULL, "malloc() failed", errno );

    tasklist->size = hostlist_get_size( hostlist );

    rv = pthread_mutex_init( &tasklist->mutex, NULL );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_init() failed", errno );

    rv = pthread_cond_init( &tasklist->cond_not_empty, NULL );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_init() failed", errno );

    rv = pthread_cond_init( &tasklist->cond_completed, NULL );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_init() failed", errno );

    // Allocate task array
    tasklist->task = malloc( tasklist->size * sizeof(task_t) );
    ASSERT_MSG_ERRNO( tasklist->task != NULL, "malloc() failed", errno );

    // Initialize all tasks
    unsigned int i;
    for ( i = 0 ; i < tasklist->size ; i++ ) {
        const char* hostname = hostlist_get_host( hostlist, i );
        task_init( &(tasklist->task[i]), hostname );
    }

    tasklist->current_index = tasklist->size;
    tasklist->completed = tasklist->size;

    return tasklist;
}

// Clean and free the tasklist object
void tasklist_destroy( tasklist_t *tasklist )
{
    int rv;
    ASSERT_MSG( tasklist != NULL, "Error: tasklist is NULL\n" );

    rv = pthread_cond_destroy( &tasklist->cond_completed );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_destroy() failed", errno );
    
    rv = pthread_cond_destroy( &tasklist->cond_not_empty );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_destroy() failed", errno );
    
    rv = pthread_mutex_destroy( &tasklist->mutex );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_destroy() failed", errno );

    // Clean all the tasks
    unsigned int i;
    for ( i = 0 ; i < tasklist->size ; i++ ) {
        task_clean( &(tasklist->task[i]) );
    }

    // Free task array
    free( tasklist->task );

    tasklist->task = NULL;
    tasklist->size = 0;
    tasklist->current_index = 0;
    tasklist->completed = 0;

    // Free tasklist structure
    free( tasklist );
}

// Get and remove a task from the tasklist
// The call will block if the tasklist is empty (until the tasklist is reseted)
task_t* tasklist_pop( tasklist_t *tasklist )
{
    int rv;
    task_t *task;
    ASSERT_MSG( tasklist != NULL, "Error: tasklist is NULL\n" );
    
    pthread_cleanup_push( (void*)&pthread_mutex_unlock, &tasklist->mutex );
    rv = pthread_mutex_lock( &tasklist->mutex );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_lock() failed", errno );

    while ( tasklist->current_index >= tasklist->size ) {
        rv = pthread_cond_wait( &tasklist->cond_not_empty, &tasklist->mutex );
        ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_wait() failed", errno );
    }

    task = &(tasklist->task[tasklist->current_index]);
    tasklist->current_index++;

    rv = pthread_mutex_unlock( &tasklist->mutex );
    pthread_cleanup_pop( 0 );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_unlock() failed", errno );

    return task;
}

// Reset the tasklist to it initial state (based on the hostlist)
void tasklist_reset( tasklist_t *tasklist )
{
    int rv;
    ASSERT_MSG( tasklist != NULL, "Error: tasklist is NULL\n" );
    
    pthread_cleanup_push( (void*)&pthread_mutex_unlock, &tasklist->mutex );
    rv = pthread_mutex_lock( &tasklist->mutex );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_lock() failed", errno );

    tasklist->current_index = 0;
    tasklist->completed = 0;
    rv = pthread_cond_broadcast( &tasklist->cond_not_empty );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_broadcast() failed", errno );
    rv = pthread_cond_broadcast( &tasklist->cond_completed );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_broadcast() failed", errno );

    rv = pthread_mutex_unlock( &tasklist->mutex );
    pthread_cleanup_pop( 0 );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_unlock() failed", errno );
}


// Count one task as completed
void tasklist_one_task_completed( tasklist_t *tasklist )
{
    int rv;
    ASSERT_MSG( tasklist != NULL, "Error: tasklist is NULL\n" );
    
    pthread_cleanup_push( (void*)&pthread_mutex_unlock, &tasklist->mutex );
    rv = pthread_mutex_lock( &tasklist->mutex );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_lock() failed", errno );

    tasklist->completed++;
    if ( tasklist->completed == tasklist->size )
    {
        rv = pthread_cond_broadcast( &tasklist->cond_completed );
        ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_broadcast() failed", errno );
    }
    
    rv = pthread_mutex_unlock( &tasklist->mutex );
    pthread_cleanup_pop( 0 );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_unlock() failed", errno );
}


// Wait for all tasks to be completed
void tasklist_wait_for_completion( tasklist_t *tasklist )
{
    int rv;
    ASSERT_MSG( tasklist != NULL, "Error: tasklist is NULL\n" );
    
    pthread_cleanup_push( (void*)&pthread_mutex_unlock, &tasklist->mutex );
    rv = pthread_mutex_lock( &tasklist->mutex );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_lock() failed", errno );

    while ( tasklist->completed < tasklist->size ) {
        rv = pthread_cond_wait( &tasklist->cond_completed, &tasklist->mutex );
        ASSERT_MSG_ERRNO( rv == 0, "pthread_cond_wait() failed", errno );
    }

    rv = pthread_mutex_unlock( &tasklist->mutex );
    pthread_cleanup_pop( 0 );
    ASSERT_MSG_ERRNO( rv == 0, "pthread_mutex_unlock() failed", errno );
}




   