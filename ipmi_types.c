#include "errors.h"
#include "ipmi_types.h"
#include <errno.h>
#include <stdlib.h>


// =======================================
// IPMI config
// =======================================

// Create with default values
ipmi_config_t* ipmi_config_create( arguments_t *arguments ) {
    
    // Allocate data structure
    ipmi_config_t* ipmi_config = malloc( sizeof(ipmi_config_t) );
    if ( ipmi_config == NULL ) {
        PRINT_ERROR_ERRNO( "malloc", errno );
        return 0;
    }
    
    // Default values in-band FreeIPMI communication
    ipmi_config->driver_type = -1;
    ipmi_config->disable_auto_probe = 0;
    ipmi_config->driver_address = 0;
    ipmi_config->register_spacing = 0;
    ipmi_config->driver_device = NULL;

    // Default values out-of-band FreeIPMI communication
    ipmi_config->protocol_version = -1;
    ipmi_config->username = arguments->username;
    ipmi_config->password = arguments->password;
    ipmi_config->k_g = NULL;
    ipmi_config->k_g_len = 0;
    ipmi_config->privilege_level = -1;
    ipmi_config->authentication_type = -1;
    ipmi_config->cipher_suite_id = -1;
    ipmi_config->session_timeout_len = 0;
    ipmi_config->retransmission_timeout_len = 0;

    // Default value for FreeIPMI workarounds
    ipmi_config->workaround_flags = 0;

    return ipmi_config;
}

// Destroy config
void ipmi_config_destroy( ipmi_config_t *ipmi_config ) {
    if (ipmi_config != NULL) free(ipmi_config);
}


// =======================================
// IPMI parameters
// =======================================

// Create with default values
ipmi_param_t* ipmi_param_create( arguments_t *arguments ) {
    
    // Allocate data structure
    ipmi_param_t* ipmi_param = malloc( sizeof(ipmi_param_t) );
    if ( ipmi_param == NULL ) {
        PRINT_ERROR_ERRNO( "malloc", errno );
        return 0;
    }   

    ipmi_param->sensor_reading_flags = 0;
    ipmi_param->sensor_reading_flags |= IPMI_MONITORING_SENSOR_READING_FLAGS_IGNORE_NON_INTERPRETABLE_SENSORS;

    ipmi_param->sensor_types = NULL;
    ipmi_param->sensor_types_length = 0;

    ipmi_param->sdr_cache_directory = "/tmp";
    
    // IPMI debug off
    ipmi_param->ipmimonitoring_init_flags = 0;
    
    return ipmi_param;
}

// Destroy config
void ipmi_param_destroy( ipmi_param_t *ipmi_param ) {
    if (ipmi_param != NULL) free(ipmi_param);
}



// =======================================
// IPMI context
// =======================================

// Destroy context
void ipmi_context_destroy( ipmi_context_t *ipmi_context ) {
    if (ipmi_context != NULL) ipmi_monitoring_ctx_destroy( ipmi_context );
}


// =======================================
// Get strings function for enum types
// =======================================

const char* sensor_type_get_string ( int sensor_type ) {
    switch(sensor_type) {
        case IPMI_MONITORING_SENSOR_TYPE_RESERVED:
            return "Reserved";
        case IPMI_MONITORING_SENSOR_TYPE_TEMPERATURE:
            return "Temperature";
        case IPMI_MONITORING_SENSOR_TYPE_VOLTAGE:
            return "Voltage";
        case IPMI_MONITORING_SENSOR_TYPE_CURRENT:
            return "Current";
        case IPMI_MONITORING_SENSOR_TYPE_FAN:
            return "Fan";
        case IPMI_MONITORING_SENSOR_TYPE_PHYSICAL_SECURITY:
            return "Physical Security";
        case IPMI_MONITORING_SENSOR_TYPE_PLATFORM_SECURITY_VIOLATION_ATTEMPT:
            return "Platform Security Violation Attempt";
        case IPMI_MONITORING_SENSOR_TYPE_PROCESSOR:
            return "Processor";
        case IPMI_MONITORING_SENSOR_TYPE_POWER_SUPPLY:
            return "Power Supply";
        case IPMI_MONITORING_SENSOR_TYPE_POWER_UNIT:
            return "Power Unit";
        case IPMI_MONITORING_SENSOR_TYPE_COOLING_DEVICE:
            return "Cooling Device";
        case IPMI_MONITORING_SENSOR_TYPE_OTHER_UNITS_BASED_SENSOR:
            return "Other Units Based Sensor";
        case IPMI_MONITORING_SENSOR_TYPE_MEMORY:
            return "Memory";
        case IPMI_MONITORING_SENSOR_TYPE_DRIVE_SLOT:
            return "Drive Slot";
        case IPMI_MONITORING_SENSOR_TYPE_POST_MEMORY_RESIZE:
            return "POST Memory Resize";
        case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_FIRMWARE_PROGRESS:
            return "System Firmware Progress";
        case IPMI_MONITORING_SENSOR_TYPE_EVENT_LOGGING_DISABLED:
            return "Event Logging Disabled";
        case IPMI_MONITORING_SENSOR_TYPE_WATCHDOG1:
            return "Watchdog 1";
        case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_EVENT:
            return "System Event";
        case IPMI_MONITORING_SENSOR_TYPE_CRITICAL_INTERRUPT:
            return "Critical Interrupt";
        case IPMI_MONITORING_SENSOR_TYPE_BUTTON_SWITCH:
            return "Button/Switch";
        case IPMI_MONITORING_SENSOR_TYPE_MODULE_BOARD:
            return "Module/Board";
        case IPMI_MONITORING_SENSOR_TYPE_MICROCONTROLLER_COPROCESSOR:
            return "Microcontroller/Coprocessor";
        case IPMI_MONITORING_SENSOR_TYPE_ADD_IN_CARD:
            return "Add In Card";
        case IPMI_MONITORING_SENSOR_TYPE_CHASSIS:
            return "Chassis";
        case IPMI_MONITORING_SENSOR_TYPE_CHIP_SET:
            return "Chip Set";
        case IPMI_MONITORING_SENSOR_TYPE_OTHER_FRU:
            return "Other Fru";
        case IPMI_MONITORING_SENSOR_TYPE_CABLE_INTERCONNECT:
            return "Cable/Interconnect";
        case IPMI_MONITORING_SENSOR_TYPE_TERMINATOR:
            return "Terminator";
        case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_BOOT_INITIATED:
            return "System Boot Initiated";
        case IPMI_MONITORING_SENSOR_TYPE_BOOT_ERROR:
            return "Boot Error";
        case IPMI_MONITORING_SENSOR_TYPE_OS_BOOT:
            return "OS Boot";
        case IPMI_MONITORING_SENSOR_TYPE_OS_CRITICAL_STOP:
            return "OS Critical Stop";
        case IPMI_MONITORING_SENSOR_TYPE_SLOT_CONNECTOR:
            return "Slot/Connector";
        case IPMI_MONITORING_SENSOR_TYPE_SYSTEM_ACPI_POWER_STATE:
            return "System ACPI Power State";
        case IPMI_MONITORING_SENSOR_TYPE_WATCHDOG2:
            return "Watchdog 2";
        case IPMI_MONITORING_SENSOR_TYPE_PLATFORM_ALERT:
            return "Platform Alert";
        case IPMI_MONITORING_SENSOR_TYPE_ENTITY_PRESENCE:
            return "Entity Presence";
        case IPMI_MONITORING_SENSOR_TYPE_MONITOR_ASIC_IC:
            return "Monitor ASIC/IC";
        case IPMI_MONITORING_SENSOR_TYPE_LAN:
            return "LAN";
        case IPMI_MONITORING_SENSOR_TYPE_MANAGEMENT_SUBSYSTEM_HEALTH:
            return "Management Subsystem Health";
        case IPMI_MONITORING_SENSOR_TYPE_BATTERY:
            return "Battery";
        case IPMI_MONITORING_SENSOR_TYPE_SESSION_AUDIT:
            return "Session Audit";
        case IPMI_MONITORING_SENSOR_TYPE_VERSION_CHANGE:
            return "Version Change";
        case IPMI_MONITORING_SENSOR_TYPE_FRU_STATE:
            return "FRU State";
        default:
            return "Unrecognized";
    }
}

const char* sensor_state_get_string (int sensor_state) {
    switch(sensor_state) {
        case IPMI_MONITORING_STATE_NOMINAL:
            return "Nominal";
        case IPMI_MONITORING_STATE_WARNING:
            return "Warning";
        case IPMI_MONITORING_STATE_CRITICAL:
            return "Critical";
        default:
            return "N/A";
    }
}

const char* sensor_units_get_string (int sensor_units) {
    switch(sensor_units) {
        case IPMI_MONITORING_SENSOR_UNITS_CELSIUS:
            return "C";
        case    IPMI_MONITORING_SENSOR_UNITS_FAHRENHEIT:
            return "F";
        case IPMI_MONITORING_SENSOR_UNITS_VOLTS:
            return "V";
        case IPMI_MONITORING_SENSOR_UNITS_AMPS:
            return "A";
        case IPMI_MONITORING_SENSOR_UNITS_RPM:
            return "RPM";
        case IPMI_MONITORING_SENSOR_UNITS_WATTS:
            return "W";
        case IPMI_MONITORING_SENSOR_UNITS_PERCENT:
            return "%";
        default:
            return "N/A";
    }
}

