#ifndef FTB_PUBLISHER_H
#define FTB_PUBLISHER_H

typedef struct ftb_context ftb_context_t;
typedef struct ftb_event ftb_event_t;

ftb_context_t* ftb_initialize();
void ftb_terminate( ftb_context_t *ftb_context );
ftb_event_t *ftb_create_sensor_event( const char *hostname, ipmi_sensor_info_t *sensor );
ftb_event_t *ftb_create_error_event( const char *hostname, const char *msg );
void ftb_event_destroy( ftb_event_t *event );
void ftb_publish_event( ftb_context_t *ftb_context, ftb_event_t *event );

#endif
