#include "hostlist.h"
#include "errors.h"
#include <stdlib.h>

struct hostlist {
    unsigned int current_size;
    unsigned int allocated_size;
    char** host;
};






void hostlist_destroy( hostlist_t *hostlist )
{
    if (hostlist == NULL) return;

    // Free all host strings
    unsigned int i;
    for ( i  = 0 ; i < hostlist->current_size ; i++ )
    {
        free( hostlist->host[i] );
        hostlist->host[i] = NULL;
    }
    hostlist->current_size = 0;
    
    // Free host array
    hostlist->allocated_size = 0;
    free( hostlist->host );
    hostlist->host = NULL;
    
    // Free hostlist structure
    free( hostlist );
}

unsigned int hostlist_get_size( const hostlist_t *hostlist )
{
    ASSERT_MSG( hostlist != NULL, "Error: hostlist is NULL\n" );
    return hostlist->current_size;
}

const char* hostlist_get_host( const hostlist_t *hostlist, const unsigned int index )
{
    ASSERT_MSG( hostlist != NULL, "Error: hostlist is NULL\n" );
    ASSERT_MSG( index < hostlist->current_size, "Error: index out of bound\n" );    
    return hostlist->host[index];
}

int hostlist_add_host( hostlist_t *hostlist, const char* host )
{
    ASSERT_MSG( hostlist != NULL, "Error: hostlist is NULL\n" );
    ASSERT_MSG( host != NULL, "Error: host is NULL\n" );
        
    // Resize host array if required
    if ( hostlist->current_size+1 > hostlist->allocated_size ) {
        // Get new size
        unsigned int new_size;
        if ( hostlist->allocated_size == 0 ) {
            new_size = 64;
        } else {
            new_size = hostlist->allocated_size * 2;
        }
        // Get new array
        char** new_host_array;
        new_host_array = realloc( hostlist->host, new_size * sizeof(char*) );
        if ( new_host_array == NULL ) {
            PRINT_ERROR_ERRNO( "realloc", errno );
            return -1;
        }
        // Update hostlist structure
        hostlist->allocated_size = new_size;
        hostlist->host = new_host_array;
    }
    ASSERT_MSG( hostlist->current_size+1 <= hostlist->allocated_size, "Internal error\n" );
    
    // Duplicate new host string
    char *host_dup = strdup( host );
    if ( host_dup == NULL ) {
        PRINT_ERROR_ERRNO( "strdup", errno );
        return -1;
    }
    
    // Add new host in the hostlist
    hostlist->host[hostlist->current_size] = host_dup;
    hostlist->current_size++;
    
    return 0;
}

char* parse_hostname( char* line ) {
    // Ignore line stating with '#'
    if ( *line == '#') {
        return NULL;
    }
    
    // Find first non null character
    while ( *line == ' ' || *line == '\t') {
        line++;
    }
    char* beg = line;
    
    // Find next space/CR/null character
    while ( *line != ' ' && *line != '\t' && *line != '\n' && *line != '\0' ) {
        line++;
    }
    
    // Set end of the hostname
    line[0] = '\0';
    
    // Ignore empty lines
    if ( line == beg ) {
        return NULL;
    }
    
    return beg;
}


#define LINE_SIZE 256
hostlist_t *hostlist_create( const char* hostfile )
{
    hostlist_t* hostlist = NULL;
    
    // Allocate hostlist
    hostlist = malloc( sizeof(hostlist_t) );
    if ( hostlist == NULL ) {
        PRINT_ERROR_ERRNO( "malloc", errno );
        goto error;
    }
    hostlist->current_size = 0;
    hostlist->allocated_size = 0;
    hostlist->host = NULL;

    // Open file
    FILE *file = fopen( hostfile, "r" );
    if ( file == NULL ) {
        PRINT_ERROR_ERRNO( "fopen('%s')", errno, hostfile );
        goto error;
    }

    char line[LINE_SIZE];
    // Read line by line
    while( fgets( line, LINE_SIZE, file ) ) {
        // Parse hostname
        char* host = parse_hostname( line );
        if ( host == NULL ) {
            continue;
        }
        
        // Add now host to the hostlist
        int rv2 = hostlist_add_host( hostlist, host );
        if ( rv2 != 0 ) {
            goto error;
        }
    }

    if ( !feof(file) ) {
        // If not end-of-file, it was an error with fgets()
        PRINT_ERROR_ERRNO( "fgets('%s')", errno, hostfile );
        goto error;
    }

    return hostlist;
    
error:
    // Cleanup
    if ( hostlist != NULL ) {
        hostlist_destroy( hostlist );
    }
    return NULL;
}


void hostlist_print( const hostlist_t *hostlist )
{
    ASSERT_MSG( hostlist != NULL, "Error: hostlist is NULL\n" );
    printf("hostlist->allocated_size = %i\n", hostlist->allocated_size);
    printf("hostlist->current_size = %i\n", hostlist->current_size);
    unsigned int i;
    for ( i = 0 ; i < hostlist->current_size ; i++ ) {
        printf("hostlist->host[%d] = %s\n", i, hostlist->host[i]);
    }
}
