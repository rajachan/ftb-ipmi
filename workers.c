#include "ftb-ipmi.h"
#include "errors.h"
#include "workers.h"
#include "ipmi_calls.h"
#include <pthread.h>
#include <stdlib.h>
#include <sys/types.h>

static unsigned int nb_threads = 0;
static pthread_t *thread_list = NULL;

static ipmi_config_t *ipmi_config = NULL;
static ipmi_param_t *ipmi_param = NULL;
static ftb_context_t* ftb_context = NULL;
static tasklist_t *tasklist = NULL;
static int keep_running = 0;

static void *worker_thread(void *arg);

int start_workers( ipmi_config_t *c, ipmi_param_t *p, ftb_context_t *f, tasklist_t *t)
{
    ASSERT_MSG( nb_threads == 0, "Internal error: worker threads already started\n" );
    if ( args.nb_threads > 0 ) {
        nb_threads = args.nb_threads;
    } else {
        nb_threads = 16;
    }
    thread_list = malloc( nb_threads * sizeof(pthread_t) );
    ASSERT_MSG_ERRNO( thread_list != NULL, "malloc() failed", errno );
    
    ipmi_config = c;
    ipmi_param = p;
    ftb_context = f;
    tasklist = t;
    keep_running = 1;
    
    unsigned int i;
    for ( i = 0 ; i < nb_threads ; i++ ) {
        int rv = pthread_create( &thread_list[i], NULL, &worker_thread, NULL);
        ASSERT_MSG_ERRNO( rv == 0, "pthread_create() failed", errno );
    }

    return 0;
}

int stop_workers()
{
    keep_running = 0;
    
    unsigned int i;
    for ( i = 0 ; i < nb_threads ; i++ ) {
        int rv = pthread_cancel( thread_list[i] );
        ASSERT_MSG_ERRNO( rv == 0, "pthread_cancel() failed", errno );
    }    

    for ( i = 0 ; i < nb_threads ; i++ ) {
        int rv = pthread_join( thread_list[i], NULL );
        ASSERT_MSG_ERRNO( rv == 0, "pthread_join() failed", errno );
    }
    
    nb_threads = 0;
    free( thread_list );
    thread_list = NULL;
    ipmi_config = NULL;
    ipmi_param = NULL;
    tasklist = NULL;
    
    return 0;
}


static void process_task( int myid, task_t *task, ipmi_config_t *ipmi_config, ipmi_param_t *ipmi_param, ipmi_context_t *ipmi_context, ftb_context_t *ftb_context )
{
    int sensor_count, i;
    sensor_count = ipmimonitoring_fetch_sensors( task->hostname, ipmi_config, ipmi_param, ipmi_context );
    if ( sensor_count > 0 ) {
        if (args.verbose>1) printf( "[Thread %d] Got %d sensors for host '%s'\n", myid, sensor_count, task->hostname );

        int sensor_state_reallocated = 0;
        if ( task->sensor_num == 0 ) {
            // Allocate sensor state array
            task->sensor_state = malloc( sensor_count * sizeof(int) );
            task->sensor_num = sensor_count;
            sensor_state_reallocated = 1;
        } else if ( task->sensor_num != sensor_count ) {
            // Re-Allocate sensor state array
            printf( "WARNING: Sensor count changed from %d to %d for host '%s'\n", task->sensor_num, sensor_count, task->hostname );
            task->sensor_state = realloc( task->sensor_state, sensor_count * sizeof(int) );
            task->sensor_num = sensor_count;
            sensor_state_reallocated = 1;
        }

        // Iterate on all sensors
        for ( i = 0 ; i < sensor_count ; i++ ) {
            ipmi_sensor_info_t sensor_info;
            memset( &sensor_info, 0, sizeof(sensor_info) );
            
            int rv = ipmimonitoring_get_current_sensor( ipmi_context, &sensor_info );
            if ( rv == 0 ) {
                if (args.verbose>2) ipmimonitoring_print_sensor( &sensor_info );

                // Only send event when previous state is known
                if ( !sensor_state_reallocated ) {

                    // Check if the state has changer
                    if (task->sensor_state[i] != sensor_info.state) {
                        ftb_event_t *event = ftb_create_sensor_event( task->hostname, &sensor_info );
                        ftb_publish_event( ftb_context, event );
                        ftb_event_destroy( event );
                    }
                }

                // Save current state
                task->sensor_state[i] = sensor_info.state;
            }
            ipmi_monitoring_sensor_iterator_next( ipmi_context );
        }
    } else {
        if (args.verbose>1) printf( "[Thread %d] Failed to read sensors for host '%s'\n", myid, task->hostname );
        ftb_event_t *event = ftb_create_error_event( task->hostname, "Failed to read sensors" );
        ftb_publish_event( ftb_context, event );
        ftb_event_destroy( event );
    }
}


static void *worker_thread(void *arg)
{
    ASSERT_MSG( ipmi_param != NULL, "Error: ipmi_param is NULL\n" );
    ASSERT_MSG( ipmi_config != NULL, "Error: ipmi_config is NULL\n" );

    int myid = (int) pthread_self();
    if (args.verbose) printf( "[Thread %d] Thread started\n", myid );

    // Initialize IPMI Monitoring library
    ipmi_context_t *ipmi_context = ipmimonitoring_initialize( ipmi_param );
    ASSERT_MSG( ipmi_context != NULL, "Error: ipmi_context is NULL\n" );
    
    while( keep_running ) {
        // Get a task
        task_t *task = tasklist_pop( tasklist );
        if (!keep_running) break;
        if (args.verbose>1) printf( "[Thread %d] Query host '%s'\n", myid, task->hostname );
            
        // Process task
        process_task( myid, task, ipmi_config, ipmi_param, ipmi_context, ftb_context );

        // Task completed
        tasklist_one_task_completed( tasklist );
    }

    free( ipmi_context );

    if (args.verbose) printf( "[Thread %d] Thread exit\n", myid );
    return NULL;
}


