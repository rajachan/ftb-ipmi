#include <stdint.h>
#include <ipmi_monitoring.h>
#include "ipmi_calls.h"
#include "errors.h"

ipmi_context_t* ipmimonitoring_initialize( ipmi_param_t *ipmi_param ) {

    ipmi_monitoring_ctx_t ctx = NULL;
    int errnum;
    int rv;

    rv = ipmi_monitoring_init ( ipmi_param->ipmimonitoring_init_flags, &errnum);
    if ( rv < 0 ) {
        char* errstr = ipmi_monitoring_ctx_strerror (errnum);
        PRINT_ERROR( "ipmi_monitoring_init: %s (%d)\n", errstr, errnum );
        return 0;
    }

    ctx = ipmi_monitoring_ctx_create();
    if ( ctx == NULL ) {
        PRINT_ERROR_ERRNO( "ipmi_monitoring_ctx_create", errno );
        return 0;
    }

    // Set SDR cache directory?
    const char* sdr_cache_directory = ipmi_param->sdr_cache_directory;
    if ( sdr_cache_directory != NULL ) {
        rv = ipmi_monitoring_ctx_sdr_cache_directory ( ctx, ipmi_param->sdr_cache_directory );
        if ( rv < 0 ) {
            PRINT_IPMI_ERROR( "ipmi_monitoring_ctx_sdr_cache_directory", ctx );
            return 0;
        }
    }

    // Read customized config file?
    rv = ipmi_monitoring_ctx_sensor_config_file( ctx, NULL );
    if ( rv < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_init", ctx );
        return 0;
    }

    return ctx;
}


int ipmimonitoring_fetch_sensors( const char* hostname, ipmi_config_t *ipmi_config, ipmi_param_t *ipmi_param, ipmi_context_t *ipmi_context ) {

    int sensor_count;
    unsigned int sensor_reading_flags = ipmi_param->sensor_reading_flags;
    unsigned int *sensor_types = ipmi_param->sensor_types;
    unsigned int sensor_types_length = ipmi_param->sensor_types_length;

    sensor_count = ipmi_monitoring_sensor_readings_by_sensor_type (
            ipmi_context, hostname, ipmi_config, sensor_reading_flags,
            sensor_types, sensor_types_length, NULL, NULL );
    if ( sensor_count < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_readings_by_sensor_type() failed for host '%s'", ipmi_context, hostname );
        return sensor_count;
    }

    return sensor_count;
}


int ipmimonitoring_get_current_sensor( ipmi_context_t *ipmi_context, ipmi_sensor_info_t *sensor )
{
    ASSERT_MSG( sensor != NULL, "Error: sensor is NULL\n" );

    // Get record ID
    sensor->id = ipmi_monitoring_sensor_read_record_id( ipmi_context );
    if ( sensor->id < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_read_record_id", ipmi_context );
        return -1;
    }

    // Get sensor name
    sensor->name = ipmi_monitoring_sensor_read_sensor_name( ipmi_context );
    if ( sensor->name == NULL ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_read_sensor_name", ipmi_context );
        return -1;
    }

    // Get sensor type
    sensor->type = ipmi_monitoring_sensor_read_sensor_type( ipmi_context );
    if ( sensor->type < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_read_sensor_type", ipmi_context );
        return -1;
    }

    // Get sensor state
    sensor->state = ipmi_monitoring_sensor_read_sensor_state( ipmi_context );
    if ( sensor->state < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_read_sensor_state", ipmi_context );
        return -1;
    }

    // Get sensor units
    sensor->units = ipmi_monitoring_sensor_read_sensor_units( ipmi_context );
    if ( sensor->units < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_read_sensor_units", ipmi_context );
        return -1;
    }

    // Get sensor reading type
    sensor->reading_type = ipmi_monitoring_sensor_read_sensor_reading_type( ipmi_context );
    if ( sensor->reading_type < 0 ) {
        PRINT_IPMI_ERROR( "ipmi_monitoring_sensor_read_sensor_reading_type", ipmi_context );
        return -1;
    }

    // Get sensor reading
    sensor->reading = ipmi_monitoring_sensor_read_sensor_reading( ipmi_context );

    return 0;
}

int ipmimonitoring_print_sensor( ipmi_sensor_info_t *sensor )
{
    const char *sensor_type_str = sensor_type_get_string( sensor->type );
    const char *sensor_state_str = sensor_state_get_string( sensor->state );
    const char *sensor_units_str = sensor_units_get_string( sensor->units );

    unsigned int MAXSTR = 20;
    char sensor_value_str[MAXSTR];
    if (sensor->reading) {
        if (sensor->reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER8_BOOL) {
            snprintf( sensor_value_str, MAXSTR, "%s", (*((uint8_t *)sensor->reading) ? "true" : "false") );
        } else if (sensor->reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER32) {
            snprintf( sensor_value_str, MAXSTR, "%u", *((uint32_t *)sensor->reading) );
        } else if (sensor->reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_DOUBLE) {
            snprintf( sensor_value_str, MAXSTR, "%.2f", *((double *)sensor->reading) );
        } else {
            snprintf( sensor_value_str, MAXSTR, "%s", "N/A" );
        }
    } else {
        snprintf( sensor_value_str, MAXSTR, "%s", "N/A" );
    }

    printf( "%20s %20s %10s %20s %5s\n", sensor->name, sensor_type_str, sensor_state_str, sensor_value_str, sensor_units_str );

    return 0;
}

