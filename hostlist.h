#ifndef HOSTLIST_H
#define HOSTLIST_H

typedef struct hostlist hostlist_t;

// Create a hostlist with all the hosts from hostfile
hostlist_t *hostlist_create( const char* hostfile );

// Clean and delete the hostfile
void hostlist_destroy( hostlist_t *hostlist );

// Get the size of the hostfile
unsigned int hostlist_get_size( const hostlist_t *hostlist );

// Get a given host from the hostlist
const char* hostlist_get_host( const hostlist_t *hostlist, const unsigned int index );

// Print the hostlist (for debugging purpose)
void hostlist_print( const hostlist_t *hostlist );

#endif
