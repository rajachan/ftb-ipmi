#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libftb.h>
#include "errors.h"
#include "ftb-ipmi.h"
#include "ipmi_types.h"
#include "ftb_publisher.h"


struct ftb_context {
    FTB_client_handle_t handle;
} ;

typedef enum {
    FTB_EVENT_IPMI_SENSOR_STATE_NOMINAL = 0,
    FTB_EVENT_IPMI_SENSOR_STATE_WARNING = 1,
    FTB_EVENT_IPMI_SENSOR_STATE_CRITICAL = 2,
    FTB_EVENT_IPMI_ERROR = 3,
    FTB_IPMI_NB_EVENTS = 4
} ftb_ipmi_event_t;

static FTB_event_info_t event_info[FTB_IPMI_NB_EVENTS] = {
    {"IPMI_SENSOR_STATE_NOMINAL",  "INFO"},
    {"IPMI_SENSOR_STATE_WARNING",  "WARNING"},
    {"IPMI_SENSOR_STATE_CRITICAL", "WARNING"},
    {"IPMI_ERROR", "WARNING"}
};

struct ftb_event {
    ftb_ipmi_event_t id;
    FTB_event_properties_t prop;
};



ftb_context_t* ftb_initialize()
{
    int rv;

    // Allocate FTB context
    ftb_context_t *ftb_context = malloc( sizeof(ftb_context_t) );
    if ( ftb_context == NULL ) {
        PRINT_ERROR_ERRNO( "malloc", errno );
        goto error;
    }
    
    // Initialize client info
    FTB_client_t ftb_cinfo;
    memset(&ftb_cinfo, 0, sizeof(ftb_cinfo));
    strcpy(ftb_cinfo.client_schema_ver, "0.5");
    strcpy(ftb_cinfo.event_space, "FTB.IPMI.FTB_IPMI");
    strcpy(ftb_cinfo.client_subscription_style, "FTB_SUBSCRIPTION_NONE");
    strcpy(ftb_cinfo.client_name, "FTB_IPMI");
    snprintf(ftb_cinfo.client_jobid, FTB_MAX_CLIENT_JOBID, "%d", getpid() );

    // Connect to FTB
    if (args.verbose>0) printf( "Connect to FTB\n" );
    rv = FTB_Connect(&ftb_cinfo, &ftb_context->handle);
    if (rv != FTB_SUCCESS) {
        PRINT_ERROR("FTB_Connect() failed with error code %d\n", rv);
        goto error;
    }

    // Declare events
    rv = FTB_Declare_publishable_events(ftb_context->handle, NULL, event_info, FTB_IPMI_NB_EVENTS);
    if (rv != FTB_SUCCESS) {
        PRINT_ERROR("FTB_Declare_publishable_events() failed with error code %d\n", rv);
        goto error;
    }

    return ftb_context;
    
error:
    // Cleanup
    if ( ftb_context != NULL ) {
        free( ftb_context );
        ftb_context = NULL;
    }
    return NULL;
};


void ftb_terminate( ftb_context_t *ftb_context )
{
    if ( ftb_context != NULL ) {
        if (args.verbose>0) printf( "Disconnect from FTB\n" );
        FTB_Disconnect( ftb_context->handle );
        free( ftb_context );
        ftb_context = NULL;
    }
}

ftb_event_t *ftb_create_sensor_event( const char *hostname, ipmi_sensor_info_t *sensor )
{
    ASSERT_MSG( hostname != NULL, "Error: hostname is NULL\n" );
    ASSERT_MSG( sensor != NULL, "Error: sensor is NULL\n" );

    // Allocate FTB event
    ftb_event_t *ftb_event = malloc( sizeof(ftb_event_t) );
    if ( ftb_event == NULL ) {
        PRINT_ERROR_ERRNO( "malloc", errno );
        return NULL;
    }

    switch(sensor->state) {
        case IPMI_MONITORING_STATE_NOMINAL:
            ftb_event->id = FTB_EVENT_IPMI_SENSOR_STATE_NOMINAL;
            break;
        case IPMI_MONITORING_STATE_WARNING:
            ftb_event->id = FTB_EVENT_IPMI_SENSOR_STATE_WARNING;
            break;
        case IPMI_MONITORING_STATE_CRITICAL:
            ftb_event->id = FTB_EVENT_IPMI_SENSOR_STATE_CRITICAL;
            break;
        default:
            ftb_event->id = FTB_EVENT_IPMI_ERROR;
            break;
    }
    ftb_event->prop.event_type = 1; // normal event



    const char *sensor_type_str = sensor_type_get_string( sensor->type );
    const char *sensor_state_str = sensor_state_get_string( sensor->state );
    const char *sensor_units_str = sensor_units_get_string( sensor->units );

    unsigned int MAXSTR = 20;
    char sensor_value_str[MAXSTR];
    if (sensor->reading) {
        if (sensor->reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER8_BOOL) {
            snprintf( sensor_value_str, MAXSTR, "%s", (*((uint8_t *)sensor->reading) ? "true" : "false") );
        } else if (sensor->reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_UNSIGNED_INTEGER32) {
            snprintf( sensor_value_str, MAXSTR, "%u", *((uint32_t *)sensor->reading) );
        } else if (sensor->reading_type == IPMI_MONITORING_SENSOR_READING_TYPE_DOUBLE) {
            snprintf( sensor_value_str, MAXSTR, "%.2f", *((double *)sensor->reading) );
        } else {
            snprintf( sensor_value_str, MAXSTR, "%s", "N/A" );
        }
    } else {
        snprintf( sensor_value_str, MAXSTR, "%s", "N/A" );
    }
    
    snprintf( ftb_event->prop.event_payload, FTB_MAX_PAYLOAD_DATA, "Hostname=%s,ID=%d,Name=%s,Type=%s,Value=%s,Units=%s,State=%s",
              hostname, sensor->id, sensor->name, sensor_type_str, sensor_value_str, sensor_units_str, sensor_state_str );

    return ftb_event;    
}

ftb_event_t *ftb_create_error_event( const char *hostname, const char *msg )
{
    // Allocate FTB event
    ftb_event_t *ftb_event = malloc( sizeof(ftb_event_t) );
    if ( ftb_event == NULL ) {
        PRINT_ERROR_ERRNO( "malloc", errno );
        return NULL;
    }

    ftb_event->id = FTB_EVENT_IPMI_ERROR;
    ftb_event->prop.event_type = 1; // normal event
    snprintf( ftb_event->prop.event_payload, FTB_MAX_PAYLOAD_DATA, "Hostname=%s,Error=%s", hostname, msg );

    return ftb_event;
}

void ftb_event_destroy( ftb_event_t *event )
{
    if (event != NULL) free(event);
}

void ftb_publish_event( ftb_context_t *ftb_context, ftb_event_t *event )
{
    FTB_event_handle_t ehandle;

    char* event_name = event_info[event->id].event_name;
    if (args.verbose>1) printf( "Publish FTB event: name='%s', payload='%s'\n", event_name, event->prop.event_payload );
    int rv = FTB_Publish( ftb_context->handle, event_name, &event->prop, &ehandle);
    if ( rv != FTB_SUCCESS ) {
        PRINT_ERROR("FTB_Publish() failed with %d\n", rv);
    }
}


