#ifndef TASKLIST_H
#define TASKLIST_H

#include "hostlist.h"

typedef struct task {
    char* hostname;
    unsigned int sensor_num;
    int *sensor_state;
} task_t;

typedef struct tasklist tasklist_t;

// Allocate and create a new taslist based on hostlist
tasklist_t* tasklist_create( hostlist_t *hostlist );

// Clean and free the tasklist object
void tasklist_destroy( tasklist_t *tasklist );

// Get and remove a task from the tasklist
// The call will block if the tasklist is empty (until the tasklist is reseted)
task_t* tasklist_pop( tasklist_t *tasklist );

// Reset the tasklist to it initial state (based on the hostlist)
void tasklist_reset( tasklist_t *tasklist );

// Count one task as completed
void tasklist_one_task_completed( tasklist_t *tasklist );

// Wait for all tasks to be completed
void tasklist_wait_for_completion( tasklist_t *tasklist );


#endif
