#ifndef IPMI_CALLS_H
#define IPMI_CALLS_H

#include "ipmi_types.h"

ipmi_context_t* ipmimonitoring_initialize( ipmi_param_t *ipmi_param );

int ipmimonitoring_fetch_sensors( const char* hostname,
                                  ipmi_config_t *ipmi_config,
                                  ipmi_param_t *ipmi_param,
                                  ipmi_context_t *ipmi_context );

int ipmimonitoring_get_current_sensor( ipmi_context_t *ipmi_context, ipmi_sensor_info_t *sensor );

int ipmimonitoring_print_sensor( ipmi_sensor_info_t *sensor );

#endif
