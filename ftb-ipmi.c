#include <sys/time.h>
#include <unistd.h>
#include "errors.h"
#include "arguments.h"
#include "hostlist.h"
#include "tasklist.h"
#include "ipmi_types.h"
#include "ipmi_calls.h"
#include "ftb_publisher.h"
#include "workers.h"

arguments_t args;




int master_loop( ipmi_config_t *ipmi_config, ipmi_param_t *ipmi_param, ftb_context_t* ftb_context, tasklist_t *tasklist ) {
    
    start_workers( ipmi_config, ipmi_param, ftb_context, tasklist );

    unsigned int iter = 0;
    while (1) {

        // Get timer, and verbose output
        struct timeval t1, t2;
        if (args.verbose>1) printf("[master_loop][iter %d] Reset tasklist\n", iter);
        if (args.timer) {
            gettimeofday( &t1, NULL );
            if (args.verbose>2) printf("[master_loop][iter %d] t1.tv_sec = %lu, t1.tv_usec = %lu\n", iter, t1.tv_sec, t1.tv_usec);
        }

        // Start queries
        tasklist_reset( tasklist );
        tasklist_wait_for_completion( tasklist );

        // Get and print timer, and verbose output
        if (args.timer) {
            gettimeofday( &t2, NULL );
            if (args.verbose>2) printf("[master_loop][iter %d] t2.tv_sec = %lu, t2.tv_usec = %lu\n", iter, t2.tv_sec, t2.tv_usec);
            double time = (t2.tv_sec - t1.tv_sec) + 1.0e-6 * (t2.tv_usec - t1.tv_usec);
            printf("[master_loop][iter %d] All tasks completed in %.3f s\n", iter, time);
        }
        if (args.verbose>1) printf("[master_loop][iter %d] All tasks completed!\n", iter);

        // Count iterations and check for the end
        iter++;
        if ( args.nb_iterations != 0 && iter >= args.nb_iterations ) break;

        // Delay between each set of queries
        sleep( args.delay );
    }

    stop_workers();

    return 0;
}



int main (int argc, char **argv)
{
    int rv, exit_code = -1;
    hostlist_t *hostlist = NULL;
    tasklist_t *tasklist = NULL;
    ipmi_config_t  *ipmi_config = NULL;
    ipmi_param_t  *ipmi_param = NULL;
    ipmi_context_t *ipmi_context = NULL;
    ftb_context_t *ftb_context = NULL;

    // Get arguments
    rv = parse_arguments( argc, argv, &args );
    if ( rv != 0 ) goto cleanup;
    if (args.verbose) print_arguments( &args );

    // Build the hostlist
    if ( args.hostfile != NULL ) {
        hostlist = hostlist_create( args.hostfile );
    } else {
        PRINT_ERROR("Error: No hosts specified. Please use --hostfile option.\n");
    }
    if ( hostlist == NULL ) goto cleanup;
    if (args.verbose>1) hostlist_print( hostlist );

    tasklist = tasklist_create( hostlist );
    if ( tasklist == NULL ) goto cleanup;
    
    // Create config
    ipmi_config = ipmi_config_create( &args );
    if ( ipmi_config == NULL ) goto cleanup;

    // Get params
    ipmi_param = ipmi_param_create( &args );
    if ( ipmi_param == NULL ) goto cleanup;

    // Initialize FTB
    ftb_context = ftb_initialize();
    if ( ftb_context == NULL ) goto cleanup;

    master_loop( ipmi_config, ipmi_param, ftb_context, tasklist );



    exit_code = 0;
cleanup:
    ftb_terminate( ftb_context );
    ipmi_context_destroy( ipmi_context );
    ipmi_config_destroy( ipmi_config );
    hostlist_destroy( hostlist );
    return exit_code;
}

