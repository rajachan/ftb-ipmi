#ifndef ARGUMENTS_H
#define ARGUMENTS_H

typedef struct argument {
    unsigned int verbose;
    unsigned int timer;
    char* hostfile;
    char* username;
    char* password;
    unsigned int nb_threads;
    unsigned int nb_iterations;
    unsigned int delay;
} arguments_t;

// Print the content of struct arguments
void print_arguments( arguments_t* arguments );

// Parse arguments from the command line and fill the struct arguments
int parse_arguments( int argc, char **argv, arguments_t* arguments );

#endif
