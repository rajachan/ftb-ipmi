#ifndef IPMI_TYPES_H
#define IPMI_TYPES_H

#include <ipmi_monitoring.h>
#include "arguments.h"

// =======================================
// IPMI config
// =======================================

typedef struct ipmi_monitoring_ipmi_config ipmi_config_t;

// Create with default values
ipmi_config_t* ipmi_config_create( arguments_t* );

// Destroy config
void ipmi_config_destroy( ipmi_config_t *ipmi_config );


// =======================================
// IPMI parameters
// =======================================

typedef struct ipmi_param {
    
    unsigned int ipmimonitoring_init_flags;
    unsigned int sensor_reading_flags;
    unsigned int *sensor_types;
    unsigned int sensor_types_length;
    char *sdr_cache_directory;

} ipmi_param_t;

// Create with default values
ipmi_param_t* ipmi_param_create();

// Destroy config
void ipmi_param_destroy( ipmi_param_t *ipmi_param );



// =======================================
// IPMI context
// =======================================

typedef struct ipmi_monitoring_ctx ipmi_context_t;

// Destroy context
void ipmi_context_destroy( ipmi_context_t *ipmi_context );


// =======================================
// IPMI sensor information
// =======================================

typedef struct ipmi_sensor_info {
    int id;
    int type;
    int state;
    int units;
    int reading_type;
    void *reading;
    char *name;
} ipmi_sensor_info_t;


// =======================================
// Get strings function for enum types
// =======================================


const char* sensor_type_get_string (int sensor_type);
const char* sensor_state_get_string (int sensor_state);
const char* sensor_units_get_string (int sensor_units);





#endif
